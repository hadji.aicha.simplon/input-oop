package co.simplon;

import java.util.UUID;

public class Task {

    private String uuid;
    private String label;
    private boolean done;

    public  Task(String label) {
        this.label = label;
        this.uuid = UUID.randomUUID().toString();
        this.done = false;
        
    }
    
    public void toggleDone() {
        done = !done;
        
    }

    public String getUuid() {
        return uuid;
    }



    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return done;
    }

    public String display() {
        if ( done == true) {
            return "☒ "+ label;
            
        }else 
        return "☐ " + label;
       
        
        
    }

    
    
}
