package co.simplon.inheritance;

import java.util.UUID;

public class Robot implements IExample, IHouseItem {

    private int battery = 100;
    private String serialNumber;
    

    public Robot(int battery) {
        this.battery = battery;
    }


    public Robot() {
        this.serialNumber = UUID.randomUUID().toString();
    }


    public int charge(int value) {
        battery += value;
        if (battery>100) {
          battery =100;  
        }
       return battery; 
    }

    protected int discharge(int value) {
        battery -= value;
        if(battery < 0){
            battery = 0;

        }
        return battery;

    }

    public boolean isOn() {
        if(battery > 0) 
        return true; 
        else{
            return false;
        } 
    }

     /**
     * Ici, on a l'implémentation de la méthode de l'interface IExemple, il faut forcément respecter 
     * la signature exact de la méthode pour que l'implémentation soit valide
     */
    @Override
    public String greeting(String name) {
        
        return "Hello Human "+name+" I am ROBOT N°"+serialNumber;
    }

    @Override
    public boolean canMove() {
        // TODO Auto-generated method stub
        discharge(10);
        return isOn();
    }

    @Override
    public void use() {
        discharge(5);
        System.out.println("Robot is discharging with expectation !");
        
    }

    @Override
    public String draw() {
        
        return "🤖";
    }


    
}
