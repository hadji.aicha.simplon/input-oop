package co.simplon.inheritance;

import java.util.ArrayList;
import java.util.List;

public class SmartHouse {

    List<IHouseItem>  items = new ArrayList<>(); 
    IHouseItem [][] grid = new IHouseItem [10][10];
    IHouseItem selected = null ;
    Integer xSelct = null; 
    Integer ySelct = null;


    public void placeItem(IHouseItem item, int x, int y){
        if( !items.contains(item) && grid[x][y] == null){
            grid[x][y] = item;
            items.add(item);
        }
    }

    public void displayHouse() {
        for (IHouseItem[] row : grid) {
            for (IHouseItem item : row) {
                if(item == null){
                    System.out.print(" .");
                }else{
                    if (item == selected){
                    System.out.print("[" +item.draw() + "]");
                    }
                    System.out.print(item.draw() );
                }
                System.out.print(" ");
                
            }
            System.out.println();
        }

        
    }

    public void select(int x , int y) {
        if(grid[x][y] != null){
            selected = grid[x][y] ;
            xSelct = x; ySelct = y;
        }    
    }

    public void  moveSelected(int x , int y){
        if(selected != null && selected.canMove() && grid[x][y] == null){
            grid[x][y] = selected;
            grid[xSelct][ySelct] = null;
            xSelct = x; ySelct = y;
        }
    }

    public void  useSelected() {
        if(selected != null){
            selected.use();
            return;
        }
        
        
    }
    
}
