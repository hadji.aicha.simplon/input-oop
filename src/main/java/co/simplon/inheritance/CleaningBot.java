package co.simplon.inheritance;

public class CleaningBot extends Robot {

    private int dustBag = 0;

    public void clean() {
        if(dustBag+5 < 100)
        dustBag += 5;
        discharge(10);
        System.out.println( "la li la lou I'm cleaning the house");
        
    }
    
    @Override
    public String draw() {
        return "🧹"+super.draw();
    }

    @Override
    public void use() {
        clean();
    }

   
}
