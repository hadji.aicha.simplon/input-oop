package co.simplon.inheritance;

public interface IExample {

    public String greeting(String name);
    
}
