package co.simplon.inheritance;

import java.util.Scanner;

public class HouseScanner {
    private SmartHouse smartHouse;
    Scanner scanner;

    public HouseScanner (SmartHouse sHouse){
        
        this.smartHouse = sHouse;
        this.scanner = new Scanner(System.in);
    }

    public void start() {
        while(true){
            smartHouse.displayHouse();
            System.out.println("""
            what do you want to do with selected item ?
              1- Move
              2- 
              3-
              """);
              int input = scanner.nextInt();

              if(input == 1) {
                  itemSelection();
              }
  
              if(input == 2) {
                  
              }
  
              if(input == 3) {
                  System.out.println("Goodbye, i'm setting your house on fire");
                  return;
              }
          }
      }
      /**
       * Méthode qui demande un x et un y et s'en sert pour sélectionner un item
       */
      private void itemSelection() {
          System.out.println("Please, select an item");
          System.out.println("x");
          int x = scanner.nextInt();
          System.out.println("y");
          int y = scanner.nextInt();
          smartHouse.select(x, y);
          selectMenu(); 
      }
      /**
       * Méthode qui affiche le menu des actions possibles lorsqu'un item est
       * sélectionné. (Il serait possible de faire une méthode générique qui marcherait
       * pour tous les menus, car si on regarde en fait c'est presqu'exactement le 
       * même code que dans la méthode start() )
       */
      private void selectMenu() {
          while(true) {
              smartHouse.displayHouse();
              System.out.println("""
              What do you want to do with selected item ?
                  1) Move
                  2) Use
                  3) Back
              """);
              int input = scanner.nextInt();
  
              if(input == 1) {
                  System.out.println("Please enter destination coordinates");
                  smartHouse.moveSelected(scanner.nextInt(), scanner.nextInt());
              }
  
              if(input == 2) {
                  smartHouse.useSelected();
              }
  
              if(input == 3) {
                  return;
              }
          }
      }

        
    }   
    

