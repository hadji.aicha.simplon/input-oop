package co.simplon.inheritance;

public class Person implements IExample, IHouseItem {
    String name;

    public String greeting(String name) {
        
        return "Hello, "+name+", I am human";
    }

    public String tellName() {
        return "i am jean";
    }

    @Override
    public boolean canMove() {
       
        return true;
    }

    @Override
    public void use() {
    name ="everybody";
    System.out.println(greeting(name));     
        
        
    }

    @Override
    public String draw() {
        return "🧍";
    }


    
}
