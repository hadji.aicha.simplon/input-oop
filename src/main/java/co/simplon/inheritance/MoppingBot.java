package co.simplon.inheritance;

public class MoppingBot extends CleaningBot {
   private boolean dryCleaning = true;
   private int waterLevel = 5;

   public boolean  toggleDryCleaning() {
      return dryCleaning = !dryCleaning;
       }

       @Override
       public void clean() {
           if(!dryCleaning && waterLevel >= 5)  {
               waterLevel -= 5;
               discharge(15);
               System.out.println("Mopping the floor");
           } else {
               //Ici, on fait en sorte d'appeler la méthode clean du parent si jamais on est en mode aspirateur.
               //Puisqu'on a redéfinie la méthode clean, la seule manière d'accéder à l'originale est en passant
               //par le mot clef super qui représente le parent
               super.clean();
   
           }
       }
   
       
   
    
}
