package co.simplon;

import java.util.InputMismatchException;
import java.util.Scanner;

public class NumberGuesser {
    
   private int answer = 50;
   private boolean over = false;
    

    public String guess (int input) {
      
      if ( answer < input ){
         return  ("it's greater than that");

      }
      if ( answer > input) {
         return ("it's lesser than that");
         
      }
          over = true;
         return ("bravo");
     
      }   
    

    public void play () {
        Scanner scantest = new Scanner(System.in);
        while (!over){
            try {

                int input = scantest.nextInt();
                String feedback = guess(input);
                System.out.println(feedback);
            } catch (InputMismatchException e) {
                System.out.println("Only numbers accepted");
                scantest.next();
            }

           
        }
        
        scantest.close();
        
    }
      /**
     * Un setter qui sera surtout utile pour les tests afin d'assigner une valeur 
     * à deviner directement dans le test 
     */
    public void setAnswer(int answer) {
        this.answer = answer;
    }
    /**
     * Un getter qui permet de savoir si le jeu est terminé ou pas
     */
    public boolean isOver() {
        return over;
    }

}



    

