package co.simplon;

import java.util.ArrayList;
import java.util.List;

public class TodoList {
    List <Task>tasks = new ArrayList<>();
   


    public String addTask ( String label) {      
      
        Task newTask = new Task(label);
        tasks.add(newTask);
        return newTask.getUuid();

    }

    public void actionTask(int index) {
        tasks.get(index).toggleDone();
        
    }
    public void clearDone() {
        
        for (int i = 0; i < tasks.size()-1; i--) {
            if(tasks.get(i).isDone()){
                tasks.remove(i);
                i--;
            }   
        }   
    }

    public void display() {
        for (Task tasklist : tasks) {
            tasklist.display();
            System.out.println( tasklist.display());
            // "\n " faire apparaitre une ligne vide
            System.out.println("\n");
            
        }
        
    }
    
}
